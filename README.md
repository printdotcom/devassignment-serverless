# To all challengers!
Very cool to take up this challenge! This challenge is for the back-end developers that love new technology like serverless or NodeJS. Depending on your level; this should take you from 2 to 6 hours. You have the choice of [Express](https://expressjs.com) or [Serverless](https://serverless.com); both are used within our technology stack.

## The challenge (also software requirements)
We want to calculate the price of a flyer. We have already made a front-end so the request body is known (requestbody.json).

1. Calculate the price of a product configuration using the prices in 'prices.json'.  
Each property has a price component and is calculated in a unique way.
*material* Price is per square mm per 1000 copies.  
*drilling_holes* Price per drilling hole per 1000 copies.  
*bundles* Price per bundle. 
*finish* Price per copy.  
*size* the size in square mm.  
 
2. Handle errors when a given configuration is not supported.


## Getting started
For the repo and get started. Good luck! If you need any help, contact [tijmen@print.com](mailto:tijmen@print.com)